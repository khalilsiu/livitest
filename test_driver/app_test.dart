// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'helpers.dart';

void main() {
  group('Test app', () {
    final textField = find.byType('TextField');
    final floatingBtn = find.byType('FloatingActionButton');
    final errorDialog = find.byValueKey('ErrorDialog');
    final arrowBackBtn = find.byValueKey('ArrowBack');

    late FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Entering a valid number will add to history', () async {
      // Enter a valid number
      const phoneNumber = '97543171';
      await driver.tap(textField);
      await driver.enterText(phoneNumber);
      await driver.tap(floatingBtn);
      final phoneNumberWidget = find.text('+852' + phoneNumber);
      final hkCode = find.text('HK');

      await delay(750);
      // Find the number in the validate history
      expect(await driver.getText(hkCode), "HK");
      expect(await driver.getText(phoneNumberWidget), "+85297543171");
    });

    test('Entering an invalid number will show error modal', () async {
      // Enter an invalid number
      const invalidNumber = '975431712222';
      const errorString =
          'Data not found: \nThe requested resource /PhoneNumbers/+852${invalidNumber} was not found';
      await driver.tap(arrowBackBtn);
      await driver.tap(textField);
      await driver.enterText(invalidNumber);
      await driver.tap(floatingBtn);
      final errorMessage = find.text(errorString);

      await delay(750);

      expect(
          await isPresent(errorDialog, driver, timeout: Duration(seconds: 2)),
          true);
      expect(await driver.getText(errorMessage), errorString);
    });
  });
}
