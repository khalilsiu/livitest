# LiviBank test

This repository contains code for the test app for LiviBank test for the Flutter frontend task.


# Tasks being done
- [x] Enter phone number to check if it is valid
- [x] Simple first screen showing an image to user
- [x] Selection box for users to choose area code
- [ ] API to detect user location
- [x] Menu for user scoll down the list of area code
- [ ] Allow keyboard typing for quick area code navigation 
- [x] Phone number pad only 
- [x] Button click to trigger API call to check the number
- [x] Front end shows validation status if the number is not correct 
- [x] List of phone number attempts shown in a List view for scrolling up and down
- [x] Back button to go back to main page

Bonus
- [x] Light dark theme
- [x] Simple responsive design layout (Add a drawer according to break point)
- [x] Environment variables are externalized in /.env (git-ignored)
- [x] Automated tests to test against positive and negative scenarios

# Testing the app
Please run `flutter drive --target=test_driver/app.dart   ` to run the test cases