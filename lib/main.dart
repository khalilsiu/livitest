import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:livitest/results.dart';
import 'package:livitest/repositories/validRepository.dart';
import 'package:flutter/material.dart';
import 'package:livitest/models/NumberValidityResponse.dart';
import 'package:livitest/config/theme/AppTheme.dart';
import 'package:livitest/widgets/areacode.dart';
import 'package:livitest/widgets/numberInput.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'models/ValidationRecord.dart';
import 'widgets/errorDialog.dart';
import 'widgets/mainBody.dart';
import 'widgets/modeButton.dart';
import 'models/Country.dart';

void main() async {
  await DotEnv.load(fileName: '.env');
  runApp(MainPage());
}

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainPageState();
}

class Bloc {
  StreamController<bool> _themeController = StreamController<bool>();
  get changeTheme => _themeController.sink.add;

  get lightThemeEnabled => _themeController.stream;
}

class _MainPageState extends State<MainPage> {
  String areacode = '🇭🇰 +852';
  List<Country> countries = [];
  String phoneNumber = '';
  List<ValidationRecord> validRecords = [];
  bool _isInAsyncCall = false;

  Future<String> _getJson() {
    return rootBundle.loadString('assets/data/country.json');
  }

  final textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    textController.addListener(_handlePhoneNumberChange);
    _getJSONdata();
  }

  void _getJSONdata() async {
    var temp = countryFromJson(await _getJson());
    setState(() {
      countries = temp;
    });
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  void handleSelect(String selected) {
    setState(() {
      areacode = selected;
    });
  }

  void _handlePhoneNumberChange() {
    setState(() {
      phoneNumber = textController.text;
    });
  }

  void _validatePhoneNumber(BuildContext context) async {
    RegExp regex = RegExp(r"\+[0-9]+$");
    Iterable<Match> matches = regex.allMatches(areacode);

    var areacodeWithoutFlag = matches.elementAt(0).group(0);

    if (areacodeWithoutFlag != null && phoneNumber.length > 0) {
      setState(() => _isInAsyncCall = true);

      String numberToValidate = areacodeWithoutFlag + phoneNumber;
      try {
        var validationRecord =
            await ValidRepository().getNumberValidity(numberToValidate);

        setState(() {
          validRecords.add(validationRecord);
          _isInAsyncCall = false;
        });
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Results(validRecords)),
        );
      } catch (Exception) {
        ShowErrorDialog(context, 'Error', Exception.toString(),
            'Please enter number again');

        var notValidRecord = ValidationRecord(
            countryCode: 'NA', phoneNumber: numberToValidate, isValid: false);

        setState(() {
          validRecords.add(notValidRecord);
          _isInAsyncCall = false;
        });
      }
    }
  }

  final bloc = Bloc();

  @override
  Widget build(BuildContext context) {
    List<Widget> childWidgets = [
      Padding(
        padding: const EdgeInsets.only(right: 10.0),
        child: Areacode(areacode, handleSelect, countries),
      ),
      Expanded(
        child: NumberInput(textController),
      ),
    ];

    return StreamBuilder(
        stream: bloc.lightThemeEnabled,
        initialData: true,
        builder: (context, snapshot) {
          var lightThemeEnabled = snapshot.data != false;
          return MaterialApp(
            theme: lightThemeEnabled
                ? AppTheme.lightTheme
                : ThemeData(
                    brightness: Brightness.dark,
                    buttonColor: Colors.grey.shade700,
                    primaryColor: Colors.grey.shade200,
                  ),
            home: Builder(
              builder: (context) {
                var hasDrawer = MediaQuery.of(context).orientation ==
                        Orientation.portrait ||
                    MediaQuery.of(context).size.width < 760;

                return ModalProgressHUD(
                    inAsyncCall: _isInAsyncCall,
                    opacity: 0.5,
                    progressIndicator: CircularProgressIndicator(),
                    child: Scaffold(
                        drawer: hasDrawer
                            ? Container(
                                width: 100,
                                child: Drawer(
                                    child: ModeButton(lightThemeEnabled, bloc)))
                            : null,
                        appBar: AppBar(
                            backgroundColor: Colors.transparent,
                            elevation: 0,
                            iconTheme: IconThemeData(
                                color: Theme.of(context).primaryColor),
                            actions: !hasDrawer
                                ? <Widget>[ModeButton(lightThemeEnabled, bloc)]
                                : []),
                        floatingActionButton: FloatingActionButton(
                          onPressed: () => _validatePhoneNumber(context),
                          child: const Icon(Icons.send),
                          backgroundColor: Color(0xff232d4b),
                        ),
                        resizeToAvoidBottomInset: true,
                        body: MainBody(hasDrawer, childWidgets)));
              },
            ),
          );
        });
  }
}
