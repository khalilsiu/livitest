// To parse this JSON data, do
//
//     final numberNotFoundResponse = numberNotFoundResponseFromJson(jsonString);

import 'dart:convert';

NumberNotFoundResponse fromJson(String str) =>
    NumberNotFoundResponse.fromJson(json.decode(str));

String toJson(NumberNotFoundResponse data) => json.encode(data.toJson());

class NumberNotFoundResponse {
  NumberNotFoundResponse({
    required this.message,
  });

  String message;

  factory NumberNotFoundResponse.fromJson(Map<String, dynamic> json) =>
      NumberNotFoundResponse(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
