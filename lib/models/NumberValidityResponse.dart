import 'dart:convert';

NumberValidityResponse numberValidityResponseFromJson(String str) =>
    NumberValidityResponse.fromJson(json.decode(str));

String numberValidityResponseToJson(NumberValidityResponse data) =>
    json.encode(data.toJson());

class NumberValidityResponse {
  NumberValidityResponse({
    required this.callerName,
    required this.countryCode,
    required this.phoneNumber,
    required this.nationalFormat,
    required this.carrier,
    required this.addOns,
    required this.url,
  });

  dynamic callerName;
  String countryCode;
  String phoneNumber;
  String nationalFormat;
  dynamic carrier;
  dynamic addOns;
  String url;

  factory NumberValidityResponse.fromJson(Map<String, dynamic> json) =>
      NumberValidityResponse(
        callerName: json["caller_name"],
        countryCode: json["country_code"],
        phoneNumber: json["phone_number"],
        nationalFormat: json["national_format"],
        carrier: json["carrier"],
        addOns: json["add_ons"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "caller_name": callerName,
        "country_code": countryCode,
        "phone_number": phoneNumber,
        "national_format": nationalFormat,
        "carrier": carrier,
        "add_ons": addOns,
        "url": url,
      };
}
