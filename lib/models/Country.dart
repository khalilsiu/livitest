import 'dart:convert';

List<Country> countryFromJson(String str) => List<Country>.from(json.decode(str).map((x) => Country.fromJson(x)));

String countryToJson(List<Country> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Country {
    Country({
        required this.dialCode,
        required this.flag,
        required this.alpha2,
        required this.name,
    });

    int dialCode;
    String flag;
    String alpha2;
    String name;

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        dialCode: json["Dial Code"],
        flag: json["Flag"],
        alpha2: json["Alpha-2"],
        name: json["Name"],
    );

    Map<String, dynamic> toJson() => {
        "Dial Code": dialCode,
        "Flag": flag,
        "Alpha-2": alpha2,
        "Name": name,
    };
}
