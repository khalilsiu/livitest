class ValidationRecord {
  final String countryCode;
  final String phoneNumber;
  final bool isValid;

  ValidationRecord(
      {required this.countryCode,
      required this.phoneNumber,
      required this.isValid});
}
