import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:livitest/models/ErrorResponse.dart';
import 'dart:convert';
import 'dart:async';

import 'AppException.dart';

class ApiBaseHelper {
  Future<dynamic> get(
      {required Uri url, required Map<String, String> headers}) async {
    var responseJson;
    try {
      final response = await http.get(url, headers: headers);

      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 404:
        var responseJson = json.decode(response.body);
        var notFoundResponse = NumberNotFoundResponse.fromJson(responseJson);
        throw NotFoundException(notFoundResponse.message);
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
