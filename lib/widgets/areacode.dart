import 'package:flutter/material.dart';
import '../models/Country.dart';

class Areacode extends StatelessWidget {
  final String _areacode;
  final Function _handleSelect;
  final List<Country> _countries;

  Areacode(this._areacode, this._handleSelect, this._countries);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35.0,
      width: 70.0,
      decoration: BoxDecoration(
          color: Theme.of(context).buttonColor,
          borderRadius: BorderRadius.circular(5)),
      child: Center(
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            value: _areacode,
            iconSize: 0.0,
            isDense: true,
            style: Theme.of(context).textTheme.subtitle1,
            // style: TextStyle(
            //     color: Colors.grey.shade900, fontWeight: FontWeight.bold),
            onChanged: (String? newValue) => _handleSelect(newValue),
            items: _countries
                .map((country) =>
                    country.flag + ' +' + country.dialCode.toString())
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
