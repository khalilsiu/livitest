import 'package:flutter/material.dart';

import '../main.dart';

class ModeButton extends StatelessWidget {
  final bool darkThemeEnabled;
  final Bloc bloc;

  ModeButton(this.darkThemeEnabled, this.bloc);
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: darkThemeEnabled
            ? const Icon(
                Icons.wb_sunny,
                color: Colors.grey,
              )
            : const Icon(
                Icons.wb_sunny_outlined,
                color: Colors.grey,
              ),
        tooltip: 'Switch to dark mode',
        onPressed: () => bloc.changeTheme(!darkThemeEnabled));
  }
}
