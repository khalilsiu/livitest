import 'package:flutter/material.dart';

class MainBody extends StatelessWidget {
  final bool hasDrawer;
  final List<Widget> childWidgets;

  MainBody(this.hasDrawer, this.childWidgets);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
            child: hasDrawer
                ? _portraitLayout(context, childWidgets)
                : _landScapeLayout(context, childWidgets)));
  }

  Widget _portraitLayout(BuildContext context, List<Widget> childWidgets) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'Validate phone number',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'To check if the number you entered is valid in the country',
              style: TextStyle(fontSize: 18, height: 1.5),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Image.asset(
                'assets/images/livilogo.png',
                height: 150,
                width: 150,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Row(children: childWidgets),
          Divider(color: Colors.black),
        ]),
      ),
    );
  }

  Widget _landScapeLayout(BuildContext context, List<Widget> childWidgets) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Padding(
          padding: const EdgeInsets.only(top: 50.0, left: 50.0),
          child: Row(
            children: [
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Validate phone number',
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        'To check if the number you entered is valid in the country',
                        style: TextStyle(fontSize: 18, height: 1.5),
                      ),
                      Row(
                        children: childWidgets,
                      ),
                      Divider(color: Colors.black),
                    ]),
              ),
              Expanded(
                child: Image.asset(
                  'assets/images/livilogo.png',
                  height: 150,
                  width: 150,
                  fit: BoxFit.contain,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
