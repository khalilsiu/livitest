import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static final ThemeData lightTheme = ThemeData(
      textTheme: lightTextTheme,
      buttonColor: Colors.grey.shade200,
      primaryColor: Colors.grey.shade500);

  static final TextTheme lightTextTheme = TextTheme(subtitle1: _subtitle1Light);

  static final TextStyle _subtitle1Light =
      TextStyle(color: Colors.grey.shade800);
}
