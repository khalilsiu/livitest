import 'package:flutter/material.dart';
import 'package:livitest/models/ValidationRecord.dart';

class Results extends StatelessWidget {
  final List<ValidationRecord> _validRecords;

  Results(this._validRecords);

  @override
  Widget build(BuildContext _context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            key: Key('ArrowBack'),
            icon: Icon(Icons.arrow_back, color: Colors.grey),
            onPressed: () => Navigator.pop(_context),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Text(
                  'Validate history',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          'Code',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        Expanded(
                            child: Text(
                          'Status',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        Expanded(
                            child: Center(
                                child: Text(
                          'Phone number',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ))),
                      ],
                    ),
                  ),
                  ..._validRecords
                      .map((record) => Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Row(
                              children: [
                                Expanded(child: Text(record.countryCode)),
                                Expanded(
                                    child: Text(
                                        record.isValid ? 'Valid' : 'Invalid')),
                                Expanded(
                                    child: Center(
                                        child: Text(
                                  record.phoneNumber,
                                ))),
                              ],
                            ),
                          ))
                      .toList()
                ],
              ),
            ],
          ),
        ));
  }
}
