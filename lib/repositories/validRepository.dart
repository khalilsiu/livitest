import 'dart:convert';

import '../constant/api_path.dart';
import '../utils/ApibaseHelpers.dart';
import '../models/NumberValidityResponse.dart';
import '../models/ValidationRecord.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class ValidRepository {
  Future<ValidationRecord> getNumberValidity(String phoneNumber) async {
    String username = env['TWILIO_ACCOUNT_SID'] as String;
    String password = env['TWILIO_AUTH_TOKEN'] as String;
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));

    var url = Uri.https(
        Strings.TWILIO_VALIDATION_ENDPOINT, 'v1/PhoneNumbers/${phoneNumber}');

    ApiBaseHelper _helper = ApiBaseHelper();

    var response = await _helper.get(url: url, headers: <String, String>{
      'authorization': basicAuth,
    });
    var numberValidityResponse = NumberValidityResponse.fromJson(response);
    return ValidationRecord(
        countryCode: numberValidityResponse.countryCode,
        phoneNumber: numberValidityResponse.phoneNumber,
        isValid: true);
  }
}
//
